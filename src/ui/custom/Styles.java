import java.awt.*;

public interface Styles {
    String fontName = "Comic Sans MS";
    Font GAME_HEADER_FONT = new Font(fontName, Font.BOLD, 32);
    Font MENU_HEADER_FONT = new Font(fontName, Font.BOLD, 20);
    Font PRIMARY_FONT = new Font(fontName, Font.PLAIN, 16);

    Color SEA_COLOR = new Color(0, 105, 148);
    Color PRIMARY_BACKGROUND_COLOR = new Color(235, 235, 235);

    Color BORDER_COLOR = new Color(56, 56, 56);
    Color HOVERED_SEA_BUTTON = new Color(11, 184, 255);

    Color REGULAR_BUTTON = new Color(204, 204, 204);
    Color REGULAR_HOVERED_BUTTON = new Color(250, 250, 250);
}
