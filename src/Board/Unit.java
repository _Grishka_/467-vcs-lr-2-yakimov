public class Unit {
    public Unit() {}
    private int hp;
    protected boolean isDestroyed = false;
    protected int count;
    public void destroy() { isDestroyed = true; }
    public boolean getIfDestroyed() {
        return isDestroyed;
    }

    public void setDestroyed(boolean destroyed) {
        isDestroyed = destroyed;
    }

    public int getCount() {
        return count;
    }
}
