import java.awt.*;
import java.security.SecureRandom;
import java.util.*;

public class Field {
    /**
     * Field's cells
     */
    private final Map<Point, Cell> cells = new HashMap<>();

    /**
     * Field's width and height
     */
    private final int fieldSize;
    sometext2;
    /**
     * Number of unbroken ships on the field
     */
    private int unbrokenShipParts = 20;

    public Field(int fieldSize) {
        this.fieldSize = fieldSize;

        sometext;
        for(int y = 0; y < fieldSize; y++) {
            for(int x = 0; x < fieldSize; x++) {
                Cell cell = new Cell();
                Point point = new Point(x, y);
                if(x > 0) {
                    point.move(x-1, y);
                    cell.setNeighbor(cells.get(point), Direction.NORTH);
                    point.move(x, y);
                }
                if(y > 0) {
                    point.move(x, y-1);
                    cell.setNeighbor(cells.get(point), Direction.WEST);
                    point.move(x, y);
                }
                cells.put(point, cell);
            }
        }

        setShips();
        setNavalMines();
        setIslands();
        setLighthouses();
    }

    public int getFieldSize() { return fieldSize; }

    public Map<Point, Cell> getFieldCells() { return cells; }

    public Cell getCellByPoint(Point point) {
        Cell cell;

        try {
            cell = cells.get(point);
        }
        catch (Exception e) { throw new IncorrectInputException("Cell with point " + point.toString() + " not found!"); }

        return cell;
    }

    public int getUnbrokenShipParts() { return unbrokenShipParts; }

    public void decreaseUnbrokenShipParts() { unbrokenShipParts--; }

    private void setShips() {
        int oneDecked = 3;
        int twoDecked = 2;
        int threeDecked = 1;

        SecureRandom random = new SecureRandom();

        for (int i = 4; i > 0; i--) {
            // start point
            int x = random.nextInt(fieldSize);
            int y = random.nextInt(fieldSize);
            boolean vertical = random.nextBoolean();

            // correct start point
            if (vertical) {
                while (y + i-1 >= fieldSize) {
                    y -= 1;
                }
            } else {
                while (x + i-1 >= fieldSize) {
                    x -= 1;
                }
            }

            boolean isFree = true;

            // check for free space
            if (vertical) {
                for (int m = y; m < y + i; ++m) {
                    boolean isNotAbleToBePlaced = cells.get(new Point(x, m)).getUnitType() != UnitType.NONE;
                    final boolean[] hasNeighbours = {false};
                    cells.get(new Point(x, m)).getNeighborCells().forEach((dir, cell) -> { if (cell.getUnitType() != UnitType.NONE) { hasNeighbours[0] = true; } });

                    if (isNotAbleToBePlaced || hasNeighbours[0]) {
                        isFree = false;
                        break;
                    }
                }
            } else {
                for (int n = x; n < x + i; ++n) {
                    boolean isNotAbleToBePlaced = cells.get(new Point(n, y)).getUnitType() != UnitType.NONE;
                    final boolean[] hasNeighbours = {false};
                    cells.get(new Point(n, y)).getNeighborCells().forEach((dir, cell) -> { if (cell.getUnitType() != UnitType.NONE) { hasNeighbours[0] = true; } });

                    if (isNotAbleToBePlaced || hasNeighbours[0]) {
                        isFree = false;
                        break;
                    }
                }
            }
            if (!isFree) {  // no free space found, retry
                wipe();
                threeDecked = 1;
                twoDecked = 2;
                oneDecked = 3;
                i = 5;
                continue;
            }

            // set the ship
            for (int n = 0; n < i; n ++) {
                cells.get(new Point(x, y)).setUnit(UnitType.SHIP_PART);
                if (vertical) {
                    y++;
                } else {
                    x++;
                }
            }

            switch(i){
                case 3:
                    if(threeDecked > 0){
                        threeDecked--;
                        i = 4;
                        continue;
                    }
                    break;
                case 2:
                    if(twoDecked > 0){
                        twoDecked--;
                        i = 3;
                        continue;
                    }
                    break;
                case 1:
                    if(oneDecked > 0){
                        oneDecked--;
                        i = 2;
                        continue;
                    }
                    break;
            }
        }
    }

    private void setNavalMines(/*possibility to choose number of mines*/) {
        SecureRandom random = new SecureRandom();

        for (int i = 0; i < /**/1; i++) {
            boolean isNotSet = true;

            do {
                int x = random.nextInt(fieldSize);
                int y = random.nextInt(fieldSize);

                boolean isAbleToBePlaced = cells.get(new Point(x, y)).getUnitType() == UnitType.NONE;
                final boolean[] hasNeighbours = {false};
                cells.get(new Point(x, y)).getNeighborCells().forEach((dir, cell) -> { if (cell.getUnitType() != UnitType.NONE) { hasNeighbours[0] = true; } });

                if (isAbleToBePlaced && !hasNeighbours[0]) {
                    cells.get(new Point(x, y)).setUnit(UnitType.NAVAL_MINE);
                    isNotSet = false;
                }
                System.out.println(x);
            }
            while (isNotSet);
        }
    }

    private void setIslands(/*possibility to choose number of islands*/) {
        SecureRandom random = new SecureRandom();

        for (int i = 0; i < /**/2; i++) {
            boolean isNotSet = true;

            do {
                int x = random.nextInt(fieldSize);
                int y = random.nextInt(fieldSize);

                boolean isAbleToBePlaced = cells.get(new Point(x, y)).getUnitType() == UnitType.NONE;
                final boolean[] hasNeighbours = {false};
                cells.get(new Point(x, y)).getNeighborCells().forEach((dir, cell) -> { if (cell.getUnitType() != UnitType.NONE) { hasNeighbours[0] = true; } });

                if (isAbleToBePlaced && !hasNeighbours[0]) {
                    cells.get(new Point(x, y)).setUnit(UnitType.ISLAND);
                    isNotSet = false;
                }
            }
            while (isNotSet);
        }
    }

    private void setLighthouses(/*possibility to choose number of lighthouses*/) {
        SecureRandom random = new SecureRandom();

        for (int i = 0; i < /**/1; i++) {
            boolean isNotSet = true;

            do {
                int x = random.nextInt(fieldSize);
                int y = random.nextInt(fieldSize);
                int z = random.nextInt(fieldSize);
                int w = random.nextInt(fieldSize);

                boolean isAbleToBePlaced = cells.get(new Point(x, y)).getUnitType() == UnitType.NONE;
                final boolean[] hasNeighbours = {false};
                cells.get(new Point(x, y)).getNeighborCells().forEach((dir, cell) -> { if (cell.getUnitType() != UnitType.NONE) { hasNeighbours[0] = true; } });

                if (isAbleToBePlaced && !hasNeighbours[0]) {
                    cells.get(new Point(x, y)).setUnit(UnitType.LIGHTHOUSE);
                    isNotSet = false;
                }
            }
            while (isNotSet);
        }
    }

    private void wipe(){
        cells.forEach(((point, cell) -> cell.setUnit(UnitType.NONE)));
    }
}